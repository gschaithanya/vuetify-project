import { createStore } from "vuex";
import itemModule from "./Modules/item.js";
import cartModule from "./Modules/cart.js";
const store = createStore({
  modules: {
    allfood: itemModule,
    cart: cartModule,
  },
});

export default store;
