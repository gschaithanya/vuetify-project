export default {
  namespaced: true,
  state() {
    return {
      cartitems: [],
      total: 0,
      qty: 0,
      tempQty: 0,
      tempAmt: 0,
    };
  },
  mutations: {
    addFoodToCart(state, payload) {
      const foodData = payload;
      const index = state.cartitems.findIndex((i) => i.foodId === foodData.id);

      if (index >= 0) {
        state.cartitems[index].qty++;
      } else {
        const newItem = {
          foodId: foodData.id,
          foodName: foodData.foodName,
          hotelName: foodData.hotelName,
          image: foodData.image,
          price: foodData.price,
          qty: 1,
        };
        state.cartitems.push(newItem);
      }
      state.qty++;
      state.total += +foodData.price;
    },
    decFoodcountCart(state, payload) {
      const foodData = payload;
      const index = state.cartitems.findIndex((i) => i.foodId === foodData.id);

      if (index >= 0 && state.cartitems[index].qty > 1) {
        state.cartitems[index].qty--;
      } else {
        Object.values(state.cartitems).filter((item) => item.qty < 1);
        state.cartitems.splice(index, 1);
      }
      state.qty--;
      state.total -= +foodData.price;
    },
    close(state, payload) {
      const foodData = payload;
      const index = state.cartitems.findIndex((i) => i.foodId === foodData.id);

      state.tempQty = state.cartitems[index].qty;
      state.tempAmt = state.cartitems[index].qty * state.cartitems[index].price;
      Object.values(state.cartitems).filter((item) => item.qty > 1);
      state.cartitems.splice(index, 1);

      state.qty = state.qty - +state.tempQty;
      state.total -= +state.tempAmt;
    },
  },

  actions: {
    addToCart(context, payload) {
      const fId = payload.id;
      const fooditems = context.rootGetters["allfood/fooditems"];
      const fooditem = fooditems.find((food) => food.id === fId);
      context.commit("addFoodToCart", fooditem);
    },

    decrement(context, payload) {
      const fId = payload.id;
      const fooditems = context.rootGetters["allfood/fooditems"];
      const fooditem = fooditems.find((food) => food.id === fId);
      context.commit("decFoodcountCart", fooditem);
    },
    closeItem(context, payload) {
      const fId = payload.id;
      const fooditems = context.rootGetters["allfood/fooditems"];
      const fooditem = fooditems.find((food) => food.id === fId);
      context.commit("close", fooditem);
    },
  },
  getters: {
    fooditems(state) {
      return state.cartitems;
    },
    totalSum(state) {
      return state.total;
    },
    quantity(state) {
      return state.qty;
    },
  },
};
