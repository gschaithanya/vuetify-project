export default {
  namespaced: true,

  state() {
    return {
      fooditems: [
        {
          id: "f1",
          foodName: "Dosa",
          hotelName: "Sree Krishna Lunch Home",
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeg_BxfV5I1WRNXRRdA4NrkhnEAFUBXfpPqQ&usqp=CAU",
          price: "50",
        },
        {
          id: "f2",
          foodName: "Idli",
          hotelName: "Sree Krishna Lunch Home",
          image:
            "https://rakskitchen.net/wp-content/uploads/2010/07/how-to-make-soft-idli-500x500.jpg",
          price: "30",
        },
        {
          id: "f3",
          foodName: "Vada",
          hotelName: "Sree Krishna Lunch Home",
          image:
            "https://pbs.twimg.com/profile_images/1592812359/Ulundhu_vadai-_400x400.jpg",
          price: "15",
        },
        {
          id: "f4",
          foodName: "Pani Puri",
          hotelName: "Sree Krishna Lunch Home",
          image:
            "https://farm8.staticflickr.com/7330/16370967900_3294c260ec_z.jpg",
          price: "30",
        },
      ],
    };
  },
  getters: {
    fooditems(state) {
      return state.fooditems;
    },
  },
};
