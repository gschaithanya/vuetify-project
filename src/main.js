import { createApp } from "vue";
import App from "./App.vue";
import router from "./router.js";

import store from "./Store/index.js";
import vuetify from "./plugins/vuetify";
import { loadFonts } from "./plugins/webfontloader";

loadFonts();

const app = createApp(App);
app.use(vuetify);
app.use(store);
app.use(router);
app.mount("#app");
